/**
 * psynJS
 * 
 * psynJS is a simple bootstrap for loading modules
 * in JavaScript from a static list found in the 
 * configuration JSON
 * 
 * @package psyn
 * @category Bootstrap
 * @version 0.4.0
 * @author VisionMise <visionmise@kuhlonline.com>
 * @copyright Geoffrey L. Kuhl 2019
 * @license Apache-2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 */



/** Global Variables **/



    /**
     * Configuration
     * psynJS Settings
     * @const {object} configuration 
     */
    const configuration     = {};



    /**
     * Imported Modules
     * Stores imported modules from configuration
     * @const {object} importedModules
     */
    const importedModules   = {}



    /**
     * Application
     * Stores all created objects and imported modules
     * from configuration
     * @const {object} application
     */
    const application       = {};



    /**
     * Error Logging
     * Log and Debugging settings
     * @const {object} logging
     */
    const logging           = {
        verbose:        false,
        debug:          false
    };



/** Functions **/



    /**
     * Load Configuration
     * Loads a configuration JSON file from a static 
     * location `psyn.json`
     * @function
     * @async
     * @returns {object|bool} Config JSON or False on error
     */
    async function loadConfiguration() {

        //Verbose Logging
        logMessage("Loading Configuration");

        //Fetch psyn config
        return await fetch('psyn.json')

            //Wait for response
            .then(response => {

                //if response is okay
                //return config json
                if (response.ok) return response.json();

                //Verbose Logging
                logError("Could not load configuration");

                //otherwise, return false
                return false;
            })

            //Upon error
            //ignore and return false
            .catch(err => {

                //Verbose Logging
                logError("Could not load configuration");

                //return error|false
                return false;
            });
        ;

    }



    /**
     * Load Required Modules
     * Loads all required modules defined in
     * configuration `modules` key
     * @async
     * @function
     * @emits module_loaded,module_not_loaded
     * @param {array} modules Imports `modules` from configuraiton object
     * @returns {object} Imported Modules
     */
    async function loadRequireModules({modules}) {

        /** 
         * Imported Modules Buffer
         * @const {object} mods List of imported mods 
         */
        const mods              = {};


        //If modules not populated
        //return void
        if (!modules || modules.length == 0) return;

        //Verbose Logging
        logMessage(`Loading ${modules.length} modules`);

        //Loop through each module in list
        for (let index in modules) {
        
            //Set module name
            const moduleName    = modules[index];

            //Set module path
            const modulePath    = `./modules/${moduleName}.js`;

            //Attempt to import module
            const mod           = await import(modulePath)

                //Wait for response
                .then(module => {

                    //if no module present
                    //return false
                    if (!module) {

                        //Verbose Logging
                        logError(`Could not load module ${moduleName}`);

                        //return false
                        return false;
                    }

                    //raise import event
                    raiseEvent('module_loaded', {
                        name:   moduleName,
                        path:   modulePath
                    });

                    //Verbose Logging
                    logMessage(`Loaded ${moduleName} module`);

                    //return imported module
                    return module;
                })

                
                //Ignore error
                //return false
                .catch(error => {

                    //raise error event
                    raiseEvent('module_not_loaded', {
                        name:   moduleName,
                        path:   modulePath,
                        error:  error
                    });

                    //Verbose Logging
                    logError(`Could not load module ${moduleName}`);

                    //return false
                    return false;
                })
            ;

            //if no mod was loaded
            //continue to next mod
            if (!mod) continue;


            //add imported module to list
            mods[moduleName]    = mod;
        }


        //return module list
        return mods;
    }



    /**
     * Instantiate Objects
     * Creates all prerequisit objects from imported modules
     * @emits object_created,object_not_created
     * @param {array}  importedModules List of imported modules
     * @param {object} application     Main Application Object
     * @param {object} configuration   Imports `instantiate` from configuration object
     * @returns void
     */
    async function instantiateObjects(importedModules, application, {instantiate}) {

        //If no list
        //or if list is empty
        //return void
        if (!instantiate || instantiate.length == 0) return;

        //Loop through each instantiate item
        //key is the module where the object can be found
        for (let moduleName in instantiate) {

            //Get module from imported modules
            //by name
            let mod         = (importedModules[moduleName]) ? importedModules[moduleName] : false;

            //if the mod is not found
            //skip and continue to next object
            if (mod == false) continue;

            //Get the object list by module name
            let objectList  = instantiate[moduleName];

            //if no list
            //or if list is empty
            //skip to next object
            if (!objectList || objectList.length == 0) continue;

            //if the module namespace does not exist in the application namespace
            //create it
            if (!application[moduleName]) application[moduleName] = {};


            //Verbose Logging
            logMessage(`Creating ${objectList.length} objects`);


            //loop through each object 
            //in object list
            for (let index in objectList) {

                //get object name from list
                let objectName  = objectList[index];

                //if no object exists by name
                //continue to next object
                if (!mod[objectName]) continue;

                //create new object
                //from imported module
                //by object name
                try {
                    let objInit     = new mod[objectName]();

                    //Verbose Logging
                    logMessage(`Created ${objectName} from ${moduleName}`);
    
                    raiseEvent('object_created', {
                        name:   objectName,
                        module: moduleName
                    });

                } catch (e) {

                    raiseEvent('object_not_created', {
                        name:   objectName,
                        module: moduleName,
                        error:  e
                    });

                    //Verbose Logging
                    logError(`Could not create object ${objectName} from ${moduleName}`);

                    continue;    
                }


                //attach object to application
                //in module namespace
                application[moduleName][objectName] = objInit;

                //next object
            }

            //next module
        }

        //return void
    }



    /**
     * Raise Event
     * Raises a custom event by name
     * @param {string} eventName Name of event to raise
     * @param {object} data Event details
     * @return {bool} Event result
     */
    async function raiseEvent(eventName, data) {

        //Verbose Logging
        logMessage(`Raised event ${eventName}`);

        //Dispatch/Raise new customer event
        //Return dispatch result
        return dispatchEvent(new CustomEvent(eventName, {detail: data}));        
    }



    /**
     * Debug
     * Prints an object or an array 
     * to the console
     * @param {object|array} anyObject Object or Array to print
     * @function
     * @returns void
     */
    function debug(anyObject) {

        //if disabled return
        if (logging.debug == false) return;

        //log object
        console.log(anyObject);
    }



    /**
     * Log Message
     * Logs a message to the console
     * @param {string} message Message to log
     * @function
     * @returns void
     */
    function logMessage(message) {

        //if logging not verbose, return
        if (logging.verbose == false) return;

        //Title
        const title     = 'psynJS';
        
        //Styled Message
        const cssMsg    = `%c${title}%c${message}`;
        
        //Title Style
        const titleCss  = `font-size:8pt;color:#0088ff;`;
        
        //Message Style
        const msgCss    = `font-size:10pt;`;
        
        //Log Styled Message
        console.log(cssMsg, titleCss, msgCss);
    }
    


    /**
     * Log Error
     * Logs an error to the console
     * @param {string} errorMessage error to log
     * @function
     * @returns void
     */
    function logError(errorMessage) {

        //if logging not verbose, return
        if (logging.verbose == false) return;
        
        //Title
        const title     = 'psynJS';
        
        //Styled Message
        const cssMsg    = `%c${title}%c${errorMessage}`;
        
        //Title Style
        const titleCss  = `font-size:8pt;color:#0088ff;`;
        
        //Message Style
        const msgCss    = `font-size:10pt;`;
        
        //Log Styled Message
        console.log(cssMsg, titleCss, msgCss);
    }



/** Procedural **/



    /**
     * Bootstrap
     * @async 
     * @function
     * @param {object} configuration   Global [ByRef] psynJS Configuration
     * @param {object} importedModules Global [ByRef] psynJS buffer for imported modules
     * @param {object} application     Global [ByRef] psynJS Application Object
     */
    (async (configuration, importedModules, application) => {


    
        /**
         * Load psynJS Configuration
         * from psyn.json
         */
        configuration   = await loadConfiguration();
    
    
    
        /**
         * Load Require pysnJS Modules
         * from `modules` key in configuration
         */
        importedModules = await loadRequireModules(configuration);


        
        /**
         * Create all required objects
         * from `instantiate` key in configuration
         */
        await instantiateObjects(importedModules, application, configuration);



        /**
         * Set Application Properties
         * for modules and configuration
         */
        application['modules']  = importedModules;
        application['config']   = configuration;



        /**
         * RaiseEvent
         * Bootstrap Done
         */
        raiseEvent('bootstrap_loaded', application);



    })(configuration, importedModules, application);